import actions from "./actions.js";
import mutations from "./mutations.js";

export default {
  state: {
    murr: null,
  },
  getters: {
    murr: ({ murr }) => {
      if (!murr) {
        return murr;
      }

      const content = JSON.parse(murr.content).blocks;

      return {
        ...murr,
        content,
      };
    },
  },
  actions,
  mutations,
};
